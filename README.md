#Quick Weight Loss Center
_____________________________________________________________

##The SunGlass Club (Landing Page) 
1. Clone this project using GIT.
2. To edit please, setup your local environment.
*[browserSync](https://browsersync.io/ "browserSync"):*
   Setup [MAMP](https://www.mamp.info/en/ "MAMP") or [WAMP](http://www.wampserver.com/en/ "MAMP")  - Edit your host file
   Edit line 18 from /gulpfile.js
*No BrowserSync: *
   Uncomment line 149 - delete line 148
3. Install NPM [installed] (https://www.npmjs.com/get-npm) & run Gulp. 		
`$ npm install `
`$ gulp `

4. Edit CSS: /sass - JS: /js/


##The SunGlass Club

Since I notice that a lot of people love sunglasses and I have direct contact from a distributor, I thought that would be amazing to have bundle subscription to get new shades.

The business model is super simple you select your plan:
SILVER 		- 1 Sunglasses per month
GOLD		- 3 Sunglasses per month
Platinum 	- 6 Sunglasses per month (one luxury brand)

For this particular landing page, I'm going to use Bootstrap 4, HTML, JS, SCSS, and GULP.

To work on some graphics, I'm going to use Sketch. 

The landing will have the options, also a questions CTA.

In the future, to complete the process must be as simple as two clicks:

1. CTA Subscription.
2. Add Basic info: email, name, gender.

Then I will capture the information:

1. If the user wants to continue the process, they need to complete their profile and of course, add their payment information to proceed the subscription. 

2. If the user doesn't continue, I will send him a reminder by email optimized by A/B testing.

